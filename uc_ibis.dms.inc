<?php

/**
 * @file
 * First Data LV redirected payment service - DMS processing functions.
 */

/**
 * DMS payment process function.
 */
function uc_ibis_process_dms() {

  // IBIS payment library.
  require_once 'merchant/Merchant.php';
  $ecomm_client_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_CLIENT_URL_TEST : UC_IBIS_ECOMM_CLIENT_URL_LIVE;
  $ecomm_server_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_SERVER_URL_TEST : UC_IBIS_ECOMM_SERVER_URL_LIVE;

  $amount = check_plain($_POST['total']);
  $ip = ip_address();
  // Use localhost IP address to make testing procedure for blakclist IP.
  // $ip = '192.168.1.2'.
  $currency = check_plain($_POST['currency']);
  $description = urlencode(check_plain($_POST['description']));
  $language = check_plain($_POST['lang']);
  $ibis_order_id = check_plain($_POST['cart_order_id']);
  $now = time();

  $merchant = new Merchant($ecomm_server_url, variable_get('uc_ibis_cert_path', ''), variable_get('uc_ibis_cert_pass', ''), 1);

  $resp = $merchant->startDMSAuth($amount, $currency, $ip, $description, $language);

  if (substr($resp, 0, 14) == 'TRANSACTION_ID') {
    $trans_id = urlencode(substr($resp, 16, 28));
    $url = $ecomm_client_url . '?trans_id=' . $trans_id . '&ibis_order_id=' . $ibis_order_id;
    $fields = array(
      'trans_id' => $trans_id,
      'amount' => $amount,
      'currency' => $currency,
      'client_ip_addr' => $ip,
      'order_id' => $ibis_order_id,
      'description' => $description,
      'language' => $language,
      'dms_ok' => '---',
      'result' => '???',
      'result_code' => '???',
      'result_3dsecure' => '???',
      'card_number' => '???',
      't_date' => $now,
      'response' => $resp,
    );
    drupal_write_record('uc_ibis_transaction', $fields);
    header("Location: $url");
  }
  else {
    watchdog('ibis', 'DMS payment make failed: !result.', array('!result' => $resp));
    $resp = htmlentities($resp, ENT_QUOTES);
    $error_fields = array(
      'error_time' => $now,
      'action' => 'startDMSAuth',
      'response' => $resp,
    );
    drupal_write_record('uc_ibis_error', $error_fields);
  }

}

/**
 * DMS payment make function.
 */
function uc_ibis_make_dms() {

  // IBIS payment library.
  require_once 'merchant/Merchant.php';
  $ecomm_server_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_SERVER_URL_TEST : UC_IBIS_ECOMM_SERVER_URL_LIVE;

  $id = check_plain($_POST['order_id']);

  $merchant = new Merchant($ecomm_server_url, variable_get('uc_ibis_cert_path', ''), variable_get('uc_ibis_cert_pass', ''), 1);

  $result = db_query("SELECT * FROM {uc_ibis_transaction} WHERE order_id='%d'", $id);

  $row = db_fetch_array($result);
  $auth_id = urlencode($_GET['trans_id']);
  $amount = check_plain($_POST['amount']) * 100;
  $currency = urlencode($row['currency']);
  $ip = urlencode($row['client_ip_addr']);
  $desc = urlencode($row['description']);
  $language = urlencode($row['language']);
  $now = time();

  $resp = $merchant->makeDMSTrans($auth_id, $amount, $currency, $ip, $desc, $language);

  if (substr($resp, 8, 2) == 'OK') {
    $trans_id = $row['trans_id'];
    $fields = array(
      'dms_ok' => 'YES',
      'makeDMS_amount' => $amount,
      'trans_id' => $trans_id,
    );
    drupal_write_record('uc_ibis_transaction', $fields, 'trans_id');
    watchdog('ibis', 'DMS payment made: !result.', array('!result' => $resp));
    drupal_set_message(check_plain('DMS payment made: !result.', array('!result' => $resp)));
    drupal_goto('admin/store/orders/' . $id);
  }
  else {
    $resp = htmlentities($resp, ENT_QUOTES);
    $error_fields = array(
      'error_time' => $now,
      'action' => 'makeDMSTrans',
      'response' => $resp,
    );
    drupal_write_record('uc_ibis_error', $error_fields);

    watchdog('ibis', 'DMS payment make failed: !result.', array('!result' => $resp));
    drupal_set_message(check_plain('DMS payment make failed: !result.', array('!result' => $resp)));
    drupal_goto('admin/store/orders/' . $id);
  }
}

/**
 * DMS payment page.
 */
function uc_ibis_make_dms_page() {

  $order = uc_order_load(arg(3));

  $output .= drupal_get_form('uc_ibis_make_dms_form', $order);

  return $output;

}

/**
 * DMS payment form.
 */
function uc_ibis_make_dms_form($form_state, $order) {

  $order = uc_order_load(arg(3));
  $trans_id = db_fetch_array(db_query("SELECT trans_id FROM {uc_ibis_transaction} WHERE order_id='%d'", $order->order_id));
  global $language;

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'order_total',
    'subject' => array(
      'order' => $order,
    ),
  );
  $options = array(
    'sign' => FALSE,
    'dec' => '.',
    'thou' => FALSE,
  );

  $data = array(
    'order_id' => $order->order_id,
    'lang' => $language->language,
  );

  $form['#action'] = base_path() . 'cart/ibis/make/dms?trans_id=' . $trans_id['trans_id'];

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }
  $form['trans_id'] = array(
    '#type' => 'textfield',
    '#disabled' => TRUE,
    '#title' => t('Transaction id'),
    '#value' => $trans_id['trans_id'],
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => check_plain(t('Amount')),
    '#value' => uc_price($order->order_total, $context, $options),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Make DMS payment'),
  );

  return $form;
}
