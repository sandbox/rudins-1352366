<?php

/**
 * @file
 * First Data LV redirected payment service pages.
 */

/**
 * Payment complete function.
 */
function uc_ibis_complete($cart_id = 0) {

  // IBIS payment library.
  require_once 'merchant/Merchant.php';
  $ecomm_server_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_SERVER_URL_TEST : UC_IBIS_ECOMM_SERVER_URL_LIVE;

  $order_id = $_REQUEST['ibis_order_id'];
  $trans_id = check_plain($_POST['trans_id']);
  $order = uc_order_load($order_id);
  $total = uc_order_get_total($order);
  $now = time();

  if ($order === FALSE) {
    return drupal_access_denied();
  }

  $merchant = new Merchant($ecomm_server_url, variable_get('uc_ibis_cert_path', ''), variable_get('uc_ibis_cert_pass', ''), 1);

  $resp = $merchant->getTransResult(urlencode($trans_id), ip_address());

  $result_code = explode(':', $resp);
  $result_code = substr($result_code[2], 1, 3);

  $failed = FALSE;
  if (substr($resp, 8, 8) == 'DECLINED') {
    $result_code = 'DECLINED';
    $failed = TRUE;
  }
  elseif (substr($resp, 8, 6) == 'FAILED') {
    $result_code = 'FAILED: ' . $result_code;
    $failed = TRUE;
  }

  if (strstr($resp, 'RESULT:')) {

    // $resp example RESULT: OK RESULT_CODE: 000 3DSECURE: NOTPARTICIPATED
    // RRN: 915300393049 APPROVAL_CODE: 705368 CARD_NUMBER: 4***********9913.
    if (strstr($resp, 'RESULT:')) {
      $result = explode('RESULT: ', $resp);
      $result = preg_split('/\r\n|\r|\n/', $result[1]);
      $result = $result[0];
    }
    else {
      $result = '';
    }

    if (strstr($resp, 'RESULT_CODE:')) {
      $result_code = explode('RESULT_CODE: ', $resp);
      $result_code = preg_split('/\r\n|\r|\n/', $result_code[1]);
      $result_code = $result_code[0];
    }
    else {
      $result_code = '';
    }

    if (strstr($resp, '3DSECURE:')) {
      $result_3dsecure = explode('3DSECURE: ', $resp);
      $result_3dsecure = preg_split('/\r\n|\r|\n/', $result_3dsecure[1]);
      $result_3dsecure = $result_3dsecure[0];
    }
    else {
      $result_3dsecure = '';
    }

    if (strstr($resp, 'CARD_NUMBER:')) {
      $card_number = explode('CARD_NUMBER: ', $resp);
      $card_number = preg_split('/\r\n|\r|\n/', $card_number[1]);
      $card_number = $card_number[0];
    }
    else {
      $card_number = '';
    }

    $fields = array(
      'order_id' => $order_id,
      'result' => $result,
      'result_code' => $result_code,
      'result_3dsecure' => $result_3dsecure,
      'card_number' => $card_number,
      'response' => $resp,
      'trans_id' => $trans_id,
    );
    drupal_write_record('uc_ibis_transaction', $fields, 'trans_id');

  }
  else {
    $resp = htmlentities($resp, ENT_QUOTES);
    $error_fields = array(
      'error_time' => $now,
      'action' => 'ReturnOkURL',
      'response' => $resp,
    );
    drupal_write_record('uc_ibis_error', $error_fields);
  }

  if ($failed == TRUE) {

    global $user;

    // Saglabā POST.
    $post_ser = serialize($_REQUEST);
    // Saglabāt responce.
    $responce_ser = serialize($resp);
    drupal_set_message(t('Payment failed. Check card nr. or expiry date.'));
    uc_order_save($order);
    uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
    uc_order_comment_save($order_id, 0, t('IBIS payment failed. Check card data and try again.') . '/' . $responce_ser);
    watchdog('ibis', 'Payment failed: !result.', array('!result' => $resp));
    uc_cart_empty(uc_cart_get_id());
    drupal_goto('user/' . $user->uid . '/order/' . $order_id . '/pay');
    exit();

  }
  else {

    uc_order_save($order);
    watchdog('ibis', 'Receiving new order notification for order !order_id.', array('!order_id' => $order->order_id));
    watchdog('ibis', 'Payment recieved: !result.', array('!result' => $resp));
    uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
    $comment = t('Paid by Credit card, order #!order.', array('!order' => check_plain($order_id)));
    uc_payment_enter($order->order_id, 'ibis', $total, 1, NULL, $comment);
    drupal_set_message(t('Your order is complete.'));
    uc_order_comment_save($order->order_id, 0, $comment, 'admin');
    uc_cart_empty(uc_cart_get_id());
    $customer_login = variable_get('uc_new_customer_login', FALSE);
    $output = uc_cart_complete_sale($order, $customer_login);
    $page = variable_get('uc_cart_checkout_complete_page', '');
    if (!empty($page)) {
      drupal_goto($page);
    }
    return $output;

  }

}

/**
 * Payment failed function.
 */
function uc_ibis_failed() {

  require_once 'merchant/Merchant.php';
  $ecomm_server_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_SERVER_URL_TEST : UC_IBIS_ECOMM_SERVER_URL_LIVE;

  $trans_id = check_plain($_POST['trans_id']);
  $error_msg = check_plain($_POST['error']);
  $now = time();

  $sql = db_query("SELECT client_ip_addr FROM {uc_ibis_transaction} WHERE `trans_id` = '%s'", $trans_id);

  $row = mysql_fetch_row($sql);
  $client_ip_addr = $row[0];

  $merchant = new Merchant($ecomm_server_url, variable_get('uc_ibis_cert_path', ''), variable_get('uc_ibis_cert_pass', ''), 1);

  $resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);
  $resp = htmlentities($resp, ENT_QUOTES);
  $resp = $error_msg . ' + ' . $resp;
  $error_fields = array(
    'error_time' => $now,
    'action' => 'ReturnFailURL',
    'response' => $resp,
  );
  drupal_write_record('uc_ibis_error', $error_fields);
  drupal_set_message(check_plain(t('Tehnical error occurred! Please contact merchant! !message'), array('!message' => $error_msg)));
  drupal_goto('cart/checkout');

}

/**
 * Reverse payment page.
 */
function uc_ibis_reverse_page() {

  $order = uc_order_load(arg(3));

  $output .= drupal_get_form('uc_ibis_reverse_form', $order);

  return $output;

}

/**
 * Reverse payment form.
 */
function uc_ibis_reverse_form($form_state, $order) {

  $order = uc_order_load(arg(3));
  $trans_id = db_fetch_array(db_query("SELECT trans_id FROM {uc_ibis_transaction} WHERE order_id='%d'", $order->order_id));
  global $language;

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'order_total',
    'subject' => array(
      'order' => $order,
    ),
  );
  $options = array(
    'sign' => FALSE,
    'dec' => '.',
    'thou' => FALSE,
  );

  $data = array(
    'order_id' => $order->order_id,
    'lang' => $language->language,
  );

  $form['#action'] = base_path() . 'cart/ibis/reverse?trans_id=' . $trans_id['trans_id'];

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }
  $form['trans_id'] = array(
    '#type' => 'textfield',
    '#disabled' => TRUE,
    '#title' => t('Transaction ID'),
    '#value' => $trans_id['trans_id'],
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#value' => uc_price($order->order_total, $context, $options),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reverse payment'),
  );

  return $form;
}

/**
 * Reverse payment to customer.
 */
function uc_ibis_reverse() {

  // IBIS payment library.
  require_once 'merchant/Merchant.php';
  $ecomm_server_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_SERVER_URL_TEST : UC_IBIS_ECOMM_SERVER_URL_LIVE;

  $id = $_GET['order_id'];
  $trans_id = urlencode($_GET['trans_id']);
  $amount = check_plain($_POST['amount']) * 100;
  $now = time();

  if ($amount == '0') {
    drupal_set_message(t('Amount invalid. <a href=\"javascript:history.go(-1)\"><< Back </a>'));
  }
  $merchant = new Merchant($ecomm_server_url, variable_get('uc_ibis_cert_path', ''), variable_get('uc_ibis_cert_pass', ''), 1);
  $resp = $merchant->reverse($trans_id, $amount);

  if (substr($resp, 8, 2) == "OK" || substr($resp, 8, 8) == "REVERSED") {

    if (strstr($resp, 'RESULT:')) {
      $result = explode('RESULT: ', $resp);
      $result = preg_split('/\r\n|\r|\n/', $result[1]);
      $result = $result[0];
    }
    else {
      $result = '';
    }

    if (strstr($resp, 'RESULT_CODE:')) {
      $result_code = explode('RESULT_CODE: ', $resp);
      $result_code = preg_split('/\r\n|\r|\n/', $result_code[1]);
      $result_code = $result_code[0];
    }
    else {
      $result_code = '';
    }

    $trans_id = urlencode($_GET['trans_id']);

    $fields = array(
      'reversal_amount' => $amount,
      'result' => $result,
      'result_code' => $result_code,
      'response' => $resp,
      'trans_id' => $trans_id,
    );
    drupal_write_record('uc_ibis_transaction', $fields, 'trans_id');
    watchdog('ibis', 'Payment reversed: !result.', array('!result' => $resp));
    drupal_set_message(t('Payment reversed.'));
    drupal_goto('admin/store/orders/' . $id);

  }
  else {
    watchdog('ibis', 'Payment reverse failed: !result.', array('!result' => $resp . $trans_id));
    drupal_set_message(t('Payment reverse failed.'));
    drupal_goto('admin/store/orders/' . $id);
    $resp = htmlentities($resp, ENT_QUOTES);
    $error_fields = array(
      'error_time' => $now,
      'action' => 'reverse',
      'response' => $resp,
    );
    drupal_write_record('uc_ibis_error', $error_fields);
  }

}
