<?php

/**
 * @file
 * First Data LV redirected payment service - SMS processing functions.
 */

/**
 * SMS payment process function.
 */
function uc_ibis_process_sms() {

  // IBIS payment library.
  require_once 'merchant/Merchant.php';
  $ecomm_client_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_CLIENT_URL_TEST : UC_IBIS_ECOMM_CLIENT_URL_LIVE;
  $ecomm_server_url = (variable_get('uc_ibis_server', '') == 'test') ? UC_IBIS_ECOMM_SERVER_URL_TEST : UC_IBIS_ECOMM_SERVER_URL_LIVE;

  $amount = check_plain($_POST['total']);
  $ip = ip_address();
  // Use localhost IP address to make testing procedure for blakclist IP.
  // $ip           = '192.168.1.2'.
  $currency = check_plain($_POST['currency']);
  $description = urlencode(check_plain($_POST['description']));
  $language = check_plain($_POST['lang']);
  $ibis_order_id = check_plain($_POST['cart_order_id']);
  $now = time();
  $cert_path = variable_get('uc_ibis_cert_path', '');
  $cert_pass = variable_get('uc_ibis_cert_pass', '');

  $merchant = new Merchant($ecomm_server_url, $cert_path, $cert_pass, 1);

  $resp = $merchant->startSMSTrans($amount, $currency, $ip, $description, $language);

  if (substr($resp, 0, 14) == "TRANSACTION_ID") {
    $trans_id = urlencode(substr($resp, 16, 28));
    $url = $ecomm_client_url . '?trans_id=' . $trans_id . '&ibis_order_id=' . $ibis_order_id;
    $fields = array(
      'trans_id' => $trans_id,
      'amount' => $amount,
      'currency' => $currency,
      'client_ip_addr' => $ip,
      'order_id' => $ibis_order_id,
      'description' => $description,
      'language' => $language,
      'dms_ok' => '---',
      'result' => '???',
      'result_code' => '???',
      'result_3dsecure' => '???',
      'card_number' => '???',
      't_date' => $now,
      'response' => $resp,
    );
    drupal_write_record('uc_ibis_transaction', $fields);
    header("Location: $url");
  }
  else {
    $resp = htmlentities($resp, ENT_QUOTES);
    watchdog('ibis', 'SMS payment make failed: !result.', array('!result' => $resp));
    $error_fields = array(
      'error_time' => $now,
      'action' => 'startsmstrans',
      'response' => $resp,
    );
    drupal_write_record('uc_ibis_error', $error_fields);
    drupal_set_message(check_plain(t('Tehnical error occurred! Please contact merchant!')));
    drupal_goto('cart/checkout');
  }
}
